Modified Ahk2Exe for AutoHotkey_H (https://github.com/HotKeyIt/ahkdll)
================================

Installation
------------

Just copy everything to your AutoHotkey\Compiler folder.

All bin, exe and dll files in AutoHotkey\Compiler and subfolders will be available in Basefile dropdown box.
This is to support different versions, so each can be saved in separate folder.


The source code to this compiler can be found at https://github.com/HotKeyIt/Ahk2Exe.